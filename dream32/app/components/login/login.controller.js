'use strict';

hozbeeapp.controller('LoginCtrl', ['$scope','$http','$location','CustomerState', function ($scope,$http,$location,CustomerState) {
	$scope.username = "";
	$scope.password = "";
	$scope.login = function(){
		$scope.headers = {
			'userid' : $scope.username
		};
		$scope.config = {
			method : 'POST',
			url : 'http://localhost:8000/customers/plogin/',
			headers : $scope.headers
		};
		$http($scope.config)
			.then(
				function(response){
					$scope.nonce = response.headers('nonce');
					$scope.hashpassword = sha224($scope.password);
					$scope.raw = $scope.hashpassword + $scope.nonce;
					$scope.credpass = sha224($scope.raw);
					$scope.credusername = btoa($scope.username);
					$scope.credusername = $scope.credusername.replace('=','');
					$scope.credusername = $scope.credusername.replace('=','');
					$scope.values = btoa( $scope.credusername + ':' + $scope.credpass ) ;
					$scope.BasicAuth = 'Basic ' + $scope.values ;
					$scope.loginheaders = {
						'Authorization' : $scope.BasicAuth,
						'nonce' : $scope.nonce
					};
					$scope.loginconfig = {
						method : 'POST',
						url : 'http://localhost:8000/customers/login/',
						headers : $scope.loginheaders
					};
					$http($scope.loginconfig)
						.then(
							function(response){
								$scope.token = response.headers('token');
								$scope.CustomerCredentials = {
									username : $scope.username,
									password : $scope.password
								};
								CustomerState.set_credentials($scope.CustomerCredentials);
								CustomerState.set_CustomerToken($scope.token);
							},
							function(response){
								console.log('Invalid Password');
								CustomerState.logout_Customer();
							}
						);
				},
				function(response){
					console.log('Invalid nonce');
					CustomerState.logout_Customer();
				}
			);
	};
	console.log('LoginCtrl created');

}]);