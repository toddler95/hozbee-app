'use strict';

hozbeeapp.config(function ($routeProvider) {
	$routeProvider
		.when('/front', {
			templateUrl: 'app/components/front/front.html',
			controller: 'FrontCtrl'
		})
		.when('/start', {
			templateUrl: 'app/components/start/start.html',
			controller: 'StartCtrl'
		})
		.when('/login', {
			templateUrl: 'app/components/login/login.html',
			controller: 'LoginCtrl'
		})
		.when('/register', {
			templateUrl: 'app/components/register/register.html',
			controller: 'RegisterCtrl'
		})
		.when('/laundry', {
			templateUrl: 'app/components/laundry/laundry.html',
			controller: 'LaundryCtrl'
		})
		.when('/foodstation', {
			templateUrl: 'app/components/foodstation/foodstation.html',
			controller: 'FoodstationCtrl'
		})
		.when('/foodsuggesion', {
			templateUrl: 'app/components/foodsuggestionfoodsuggesion.html',
			controller: 'FoodsuggesionCtrl'
		})
		.when('/fooddetails', {
			templateUrl: 'app/components/fooddetails/fooddetails.html',
			controller: 'FooddetailsCtrl'
		}).
		when('/footer', {
			templateUrl: 'app/common/footer/footer.html',
			controller: 'FooterCtrl'
		})
		.when('/laundryfront', {
			templateUrl: 'app/components/laundryfront/laundryfront.html',
			controller: 'LaundryfrontCtrl'
		})
		.when('/laundrycatalogue', {
			templateUrl: 'app/components/laundrycatalogue/laundrycatalogue.html',
			controller: 'LaundrycatalogueCtrl'
		})
		.when('/laundryorders', {
			templateUrl: 'app/components/laundryorders/laundryorders.html',
			controller: 'LaundryordersCtrl'
		})
		;
	
});