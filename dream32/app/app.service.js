'use strict';

hozbeeapp.factory('CustomerState', [ '$window' , function ($window) {
	return {
		// Get cookies
		credentials : function(){
			var CustomerCredentials = $window.localStorage.getItem('CustomerCredentials');
			var credentials = JSON.parse(CustomerCredentials);
			return credentials;			
		},
		token : function(){
			var token = $window.localStorage.getItem('CustomerToken');
			return token;
		},
		islogged : function(){
			var isLogged = $window.localStorage.getItem('CustomerIsLoged');
			return isLogged;
		},
		// Set cookies
		set_credentials : function(UserData){
			$window.localStorage.setItem('CustomerCredentials',JSON.stringify(UserData));
			return true;
		},
		set_CustomerToken : function(UserToken){
			$window.localStorage.setItem('CustomerToken',UserToken);
			$window.localStorage.setItem('CustomerIsLoged',true);
			return true;
		},
		logout_Customer : function(){
			$window.localStorage.removeItem('CustomerCredentials');
			$window.localStorage.removeItem('CustomerToken');
			$window.localStorage.setItem('CustomerIsLoged',false);
			return true;
		}
	};
}]);